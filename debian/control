Source: jsurf-alggeo
Section: math
Priority: optional
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Jerome Benoit <calculus@rezozer.net>
Rules-Requires-Root: no
Build-Depends: debhelper-compat (=13), javahelper, maven-repo-helper, help2man
Build-Depends-Indep:
 default-jdk,
 antlr3, libvecmath-java, libcommons-io-java, libcommons-cli-java,
 rdfind, symlinks
Standards-Version: 4.6.1
Homepage: https://imaginary.org/program/jsurf
Vcs-Git: https://salsa.debian.org/science-team/jsurf-alggeo.git
Vcs-Browser: https://salsa.debian.org/science-team/jsurf-alggeo

Package: jsurf-alggeo
Architecture: all
Depends: ${java:Depends}, ${misc:Depends}
Suggests: graphicsmagick-imagemagick-compat
Description: Java based visualization library for real algebraic geometry
 jsurf is a Java library to visualize some real algebraic geometry.
 It is and can be used as the visualization component in Java programs.
 .
 Beside the library, this package also provides a simple script driven
 tool for drawing real algebraic geometric surfaces, as well as some
 script samples. (This simple tool is similar to, but not compatible with,
 the tool surf-alggeo distributed within the package surf-alggeo.)
 .
 jsurf is free software distributed under the Apache 2.0 License.
